Программный комплекс предназначен для расчета статистики, мониторинга и прогнозирования бизнес-метрик работы ЕГАИС.  
  
Состав histmonforecast вер. 0.0.1 следующий:  
1.	histmonforecast/0_installpip.ipynb – инсталяция необходимых библиотек и модулей phyton  
2.	histmonforecast/1_UI-config.ipynb – создание и редактирование конфигурационных данных.  
3.	histmonforecast/2_createTables.ipynb – создание выходных таблиц в Postgresql, пример конфигурирования CRON, запуск jupyter notebook как сервис.  
4.	histmonforecast/getHMFtables.ipynb – скрипт для отладки просмотра содержимого выходных таблиц в БД Postgresql.  
5.	histmonforecast/!UI.ipynb – Основной интерфейс для заполнения и администрирования выходных таблиц Postgresql.  
6.	histmonforecast/README.md – Краткое описание histmonforecast вер. 0.0.1  
7.	histmonforecast/jupyter.service – файл сервиса. Генерируется автоматически из 2_createTables.ipynb.  
8.	histmonforecast/conf.json – конфигурационный файл. Может быть сгенерирован из 1_UI-config.ipynb.  
9.	histmonforecast/push.sh – скрипт для обновления git-хранилища histmonforecast  
10.	histmonforecast/gap_10.ipynb – основной скрипт для выполнения расчетов. На основании этого скрипта с помощью 2_createTables.ipynb или update.sh генерируется gap_10.py.   
11.	histmonforecast/gap_10.py - основной py-скрипт для выполнения расчетов.  
12.	histmonforecast/run.sh – скрипт для пробного запуска gap_10.py с соответствующим виртуальным окружением.  
13.	histmonforecast/update.sh – скрипт для обновления локальной папки histmonforecast с git-сервера с сохранением конфигурационного файла.  
14.	histmonforecast/doc/Программный комплекс histmonforecast v 0.0.1.docx – текущая документация  
15.	histmonforecast/grafana/hmf01.json – файл для импорта в ПО Grafana c настроенной визализацией данных  
Инструкция по развертыванию программного комплекса скриптов представлена ниже.    


## 2021-11-26
добавление документации и релиз версии 0.0.1

## 2021-11-25
* доработаны графики для графаны
## 2021-11-22
* добавлен скрипт run.sh - для проверки завпуска аналогично CRON c текущим временем
* добавлен getHMFtables.ipynb - для отладки данных в таблицах
Проверил запуск на нормальных данных. Пропуски в мониторе наблюдаются на неправильных входных данных.
## 2021-11-21
* изменен UI реализовано ближе к имитации запуска CRON
* реализована отладка
* gap-10 поправлено время конца расчета и начала прогнозирования
* добавлен файл автоматического обновления 'mv conf.json ../conf.json && rm -rf ..?* .[!.]* * && cd ..&&  git clone https://gitlab.com/e1567/histmonforecast.git && mv conf.json histmonforecast/conf.json && cd histmonforecast && echo update - OK && sudo systemctl restart jupyter.service'
* добавлен .gitignore для исключения debug папки

# Инструкция по развертыванию программного комплекса


# Установка PostgreSQL-14
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

sudo apt-get update

sudo apt-get -y install postgresql-14

sudo apt-get -y install postgresql-server-dev-all

sudo nano /etc/postgresql/14/main/postgresql.conf

	listen_addresses = '*'

sudo nano /etc/postgresql/14/main/pg_hba.conf

	host    ega_db          ega             10.0.2.0/24            md5 
sudo systemctl restart postgresql
 
# Установка timescaledb

sudo apt install postgresql-common

sudo sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh

sudo sh -c "echo 'deb [signed-by=/usr/share/keyrings/timescale.keyring] https://packagecloud.io/timescale/timescaledb/ubuntu/ $(lsb_release -c -s) main' > /etc/apt/sources.list.d/timescaledb.list"

wget --quiet -O - https://packagecloud.io/timescale/timescaledb/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/timescale.keyring

sudo apt-get update

sudo apt –y install timescaledb-2-postgresql-14

sudo timescaledb-tune

sudo service postgresql restart
 
# Настройка доступа к СУБД
sudo su postgres 

psql

	\password postgres
	create user ega with password 'Ega_123';
	create database ega_db;
	GRANT ALL PRIVILEGES ON DATABASE "ega_db" to ega;
	\c ega_db
	GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO "ega";
	CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
	\q
exit

psql postgresql://ega:Ega_123@localhost:5432/ega_db

select * from pg_extension;

 
# Устанавливаем Grafana
sudo apt-get install -y adduser libfontconfig1

wget https://dl.grafana.com/enterprise/release/grafana-enterprise_8.2.3_amd64.deb

sudo dpkg -i grafana-enterprise_8.2.3_amd64.deb

sudo systemctl enable grafana-server

sudo systemctl start grafana-server

http://localhost:3000

admin

admin

 
 
# Программный комплекс на базе phyton (jupyter notebook
1. Устанавливаем python3, pip, virtualenv

sudo apt –y install python3-pip python3-dev python3-venv

2. Переходим в нужный каталог и создаем виртуальное окружение, активируем его

python3 -m venv env 

source env/bin/activate

3. Скачиваем Программный комплекс

git clone https://gitlab.com/e1567/histmonforecast.git

cd histmonforecast/

4. У станавливаем jupyter notebook

python3 -m pip install jupyter notebook

5. Создаем файл конфигурации и добавляем настройки для доступа через сеть

jupyter notebook --generate-config

nano /home/ega/.jupyter/jupyter_notebook_config.py

	c.NotebookApp.open_browser = False
	c.NotebookApp.port = 9999
	c.NotebookApp.allow_origin = '*'
	c.NotebookApp.allow_remote_access = True
	c.NotebookApp.ip = '0.0.0.0'
6. Задать новый пароль и запускаем юпитер из папки программного комплекса

jupyter notebook password

jupyter notebook


7. Далее по тексту ввелсти вместо localhost на IP-адрес сервера с jupyter. Открываем браузером блокнот для инсталляции необходимых пакетов и запускаем на исполнение все ячейки. Обращаем внимания на ошибки...
http://localhost:9999/notebooks/0_installpip.ipynb
(или http://localhost:9999/tree? И выбрать 0_installpip.ipynb)
 
8. Останавливаем и запускаем юпитер. Идем на домашнюю страницу по ссылке Nbextensions
http://х.х.х.х:9999/tree?#nbextensions_configurator

убираем  disable configuration

дабавляем Hide input all (должен добавиться глаз в блокнотах после обновления)

дабавляем Collapsible Headings
 
9. Открываем браузером блокнот для задания конфигурации, запускаем на исполнение все ячейки (двойная стрелка или каждую ячейку комбинацией cntr+Enter).
http:// х.х.х.х:9999/notebooks/1_UI-config.ipynb

Редактируем conf.json (можно по ссылке средствами юпитера) для доступа к БД clickhouse и PostgreSQL
 
10. Открываем браузером блокнот 2_createTables и запускаем. Скрипт Создает таблицы в PostgreSQL, настраивает их как гипертаблицы, помогает настроить юпитер для запуска как сервис, генерирует py-скрипт, помогает сконфигурировать cron для регулярного запуска py-скрипта. Обращайте внимание на оранжевые сообщения
http://localhost:9999/notebooks/2_createTables.ipynb

11. Открываем браузером блокнот !UI.ipynb. Интерфейс позволяет чистить тыблицы если нужно, расчитать статистику за первую неделю (или выбранный диапазон дат). Запустить имитацию работы по прошлым периодам до настоящего времени.
http://localhost:9999/notebooks/!UI.ipynb
 
# Отображение результатов в графическом виде
1. Заходим в графану http://localhost:3000 и импортируем в Grafana dashboard из  histmonforecast/grafana/hmf01.json

2. Сверху можно задать интервал времени для группировки данных
 
3. на первом графике отображается сводная таблица – рассчитанная статистика, текущие данные за 4часа (monitor подсвечены фиолетовым), прогноз на 24 часа (forecast подсвечены оранжевым). Также красным подсвечены данные не в режиме (notnormal) Например, места где не было ТТН. 
* m0-1_pc (процент ТТН обработанных (полное время включающее время сборки) за минуту); 
* m1-15_pc (процент ТТН обработанных от 1 до 15 минут); 
* m15-60_pc (процент ТТН обработанных от 15 до 60 минут) ; 
* m60-max_pc (процент ТТН обработанных от 60 минут до максимума (не более 4-ех часов) ) ; 
* allDocs_cnt (Кол-во всех обработанных цепочек  (цепочка = один документ на вход, один или несколько на выход) ;
* wb_cnt (кол-во ТТН обработанных стандартно (2 тикета, for2reginfo, waybill_v34 последовательность не важна); 
* wbbad_pc (процент ТТН обработанных по нестандартной цепочке (2 тикета, for2reginfo, waybill_v34).

 
4. На графике «Потоки» отображены кол-во документов за минимум полчаса (масштабируется от выбранного интервала). 
* a_cnt – таблица tin_doc; 
* b_cnt – таблица tinout_doc; 
* c_cnt – таблица tout_doc; 
* wbbad_cnt – кол-во ТТН которые прошли обработку отличную от 4-ех выходных документов (2 тикита, form2reginfo, waybill_v34).
 
5. Табличка интервалы не в режиме отображает пояснения по периодам обработанным не в режиме «normal». Эти данные игнорируются при обучении.
 
6. Таблица «Времена» показывает квантиль (процентиль) 98% (время меньшее или равное соответствующим 98% ТТН). 
* dt_sb – время сборки,
* dt_obr – время обработки,
* dt_full – полное время,
*dt0 – Время после сборки до первого выходного документа (обычно это тикет).
 
7. «Разность по модулю» помогает оценить величину отличия прогнозирования за каждый период по параметру m0-1_pc (процент ТТН обработанных за одну минуту) 
* abs delta  - разность hist и forecast
* hist – величина m0-1_pc из статистики
* forecast – величина m0-1_pc при прогнозировании
 
8. «Время работы алгоритма» позволяет оценить время затраченное при расчетах.
* dt_full – полное время (расчет статистики и пронозирование)
* dt_fore – время затраченное на прогнозирование
 
 
# Дополнтельно
Если в программный комплекс были внесены изменения, то для загрузки изменений в гит можно использовать:
git status
git branch -M main
git add .
git commit -m "DESCRIBE"
git push -uf origin main
